# DevOpsCourse

Projeto criado para aprendizado na disciplina IMD0190 - TÓPICOS ESPECIAIS EM BUSINESS INTELIGENCE E ANALYTICS 2, da residência em BI - Turma JF UFRN. 

##### Descrição da atividade:

Para o trabalho final você deverá criar um projeto que coloque em prática os conteúdos vistos no curso. 

##### Itens que devem ser cobertos:

- Criar um repositório Git no gitlab
- Criar automação com pipeline (ou afins)
- Criar imagens Docker, quando couber
- Deploy automático, quando couber.
- Uso do Kubernetes para deploy (desejável)
